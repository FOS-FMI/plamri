
% ****************************************************
% ***** Open and read parameters for Bruker data *****
% ****************************************************

   function I = Fpv_msme_ft2dbc09(name, num, win, bc);

% opens an Avance fid file with the given name and number
%    then performs 2D ft using optional window function;
%    returns the transformed data
%
% note: assumes 32-bit data

   format compact;

   number     = num2str(num);
   fidname    = strcat(name,'/',number,'/fid');
   acqpname   = strcat(name,'/',number,'/acqp');

   fid        = fopen(fidname,'r','l');
   acqp       = fopen(acqpname,'rt');

   if (fid == -1)
      error('Data not found');
   end

   epi = 0;
   while (feof(acqp) == 0)
      line = fgetl(acqp);
      if (isempty(findstr(line,'$ACQ_size')) == 0)
         line = fgetl(acqp);
         s = sscanf(line,'%d');
      end
      if (isempty(findstr(line,'$L=')) == 0)
         line = fgetl(acqp);
         l = sscanf(line,'%d');
      end
      if (isempty(findstr(line,'$ACQ_obj_order=')) == 0)
         raworder = [];
         line = fgetl(acqp);
         while (isempty(findstr(line,'##$')) ~= 0)
            tmp      = sscanf(line,'%d');
            raworder = cat(1,raworder,tmp);
            line     = fgetl(acqp);
         end;
      end
      if (isempty(findstr(line,'$NECHOES')) == 0)
         nechoes = sscanf(line,'##$NECHOES= %d',1);
      end;
      if (isempty(findstr(line,'$NSLICES')) == 0)
         nslices = sscanf(line,'##$NSLICES= %d',1);
      end;
      if (isempty(findstr(line,'$NR')) == 0)
         nr = sscanf(line,'##$NR= %d',1);
      end;
   end

   d1 = s(1)/2;
   d2 = s(2);
   d3 = nslices;
   d4 = nechoes;
   d5 = nr;

   disp(strcat(name,', experiment #',number));
   disp(strcat('   points in read dimension (d1):',num2str(d1)));
   disp(strcat('            phase dimension (d2):',num2str(d2)));
   disp(strcat('            slice dimension (d3):',num2str(d3)));
   disp(strcat('      number of echo images (d4):',num2str(d4)));
   disp(strcat('      number of repetitions (d5):',num2str(d5)));

% *************************************
% ***** 3D FT for opened SER file *****
% *************************************

   if ((d1==0)|(d2==0))
      error('Data not 2D');
   end

   N   = ceil(s(1)/256)*256;
   vol = zeros(N*d4,d2*d3);
   mat = zeros(d1,d2,d3,d4,d5);

% load data

   disp('   loading data...');
   frewind(fid);
   for t=1:d5
      [vol,cnt] = fread(fid,[N*d4 d2*d3],'int32');
      if (cnt < N*d2*d3*d4)
         d5 = t-1;
         break;
      end;
      vol = permute(reshape(vol,N,d4,d3,d2),[1 4 3 2]);
      mat(:,:,:,:,t) = squeeze(vol(1:2:2*d1,:,:,:)) + ...
                       i*squeeze(vol(2:2:2*d1,:,:,:));
   end;

% reorder slices

   tmp   = floor(raworder/d4);
   order = tmp(d4:d4:length(tmp));
   if (d3 > 1)
      for t = 1:d5
         for n = 1:d4
            mat(:,:,(order+1),n,t) = mat(:,:,:,n,t);
         end;
      end;
   end;

   % perform baseline correction, if specified

   if (nargin < 4)
      bc = 0;
   end;
   if (bc == 1)
      disp('   performing baseline correction...');
      mat = Fmat_bc(mat,1,0.3); % was 0.05
   end;

% multiply by window function

   if (nargin == 2)
      [x,y,z] = ndgrid(pi/d1:pi/d1:pi,pi/d2:pi/d2:pi,1:d3);
      win     = (sin(x).*sin(y)).^2;
   end;

%  disp('   applying window function...');
%  for t = 1:d5
%     for n = 1:d4
%        mat(:,:,:,n,t) = mat(:,:,:,n,t) .* win;
%     end;
%  end;

% 2D magnitude FT of the data

   disp('   transforming data...');
   for t=1:d5
      for n=1:d4
         for k=1:d3
            I(:,:,k,n,t) = abs(fftshift(fft2(mat(:,:,k,n,t),d1,d2)));
         end;
      end;
   end;
   I = squeeze(I);

% ***************************
% ***** close all files *****
% ***************************

   fclose('all');

