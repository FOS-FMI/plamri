% ProcessR2
%
% Fit transverse relaxation rates (R2) for a multiecho
% T2-weighted scan of a multiwell plate, starting from the raw FID.
%
%
% Syntax:
% [R2 chosenwells] = ProcessR2(datadir, scan, [ROIshape], [upsidedown], [slice], [offset], [baseline])
%
% Parameters:
%
% datadir = root directory name for Bruker data, given as a string, of
% course.
% e.g. 'C:\Users\Victor\Documents\My Dropbox\Lab Data
% (Jasanoff)\MRI\vl092512.g81'
%
% scan = scan number to process
%
% slice = slice number to process (OPTIONAL, DEFAULT = 1)
%
% ROIshape = either 'disk' or 'square'.  This option determines whether the
% function uses a disk-shaped or filled square-shaped ROI mask on
% recognized plate wells.  (OPTIONAL, DEFAULT = 'square')
%
% NOTE: The dimensions of these ROIs are sadly hard-coded right now.
% They're based on the usual ROI sizes for 384 circle-well or square-well
% plates with a 5 cm scan field-of-view.  Edit parameter lines to change.
%
% upsidedown = 0 or 1. Use this parameter if you put the
% plate into the scanner upside down.  This will clean up your little mess.
% (OPTIONAL, DEFAULT = 0);
%
% offset = [X Y]. An array containing a pixel offset that will be used to
% translate the ROI from the centroid of recognized wells. This is useful
% for vertical slices of wells. (OPTIONAL, DEFAULT = [0 0])
%
% baseline = 0 or 1. This determines whether the function will attempt to
% subtract baseline intensity values from a dilated ROI based on recognized
% wells in the reconstructed magnitude image. (OPTIONAL, DEFAULT = 0).
%
% The function will automatically write a comma-delimted csv file in the
% data directory specified in the first parameter.  The first column is the
% assigned well number for recognized wells.  The remaining columns are R2
% rate estimates in 1/sec units based on odd echoes, even echoes, and
% all echoes, respectively.
%
% ----------------------------------------------------------------------
% Victor Lelyveld
% Jasanoff Lab
% 2012
% ----------------------------------------------------------------------

function [R2 chosenwells] = ProcessR2(datadir, scan, varargin)

    %Parse arguments and set default parameters
    if nargin < 2
        disp('Error: Too few arguments.')
    end
    if nargin >= 3 && ~isempty(varargin{1})
        ROIshape = varargin{1};
    else
        ROIshape = 'square'; %DEFAULT to square shaped ROI
    end
    if nargin >=4 && ~isempty(varargin{2})
        UPSIDEDOWN = varargin{2};
    else
        UPSIDEDOWN = 0; %DEFAULT to right side up
    end
    if nargin >= 5 && ~isempty(varargin{3})
        slice = varargin{3};
        if ~slice
            slice = 1;
        end
    else
        slice = 1;
    end
    if nargin >= 6 && ~isempty(varargin{4})
        offset = varargin{4};
    else
        offset = [0 0];
    end
    if nargin == 7 && ~isempty(varargin{5})
        BASELINE = varargin{5};
    else
        BASELINE = 0;
    end
    if nargin > 7
        disp('Error: Too many arguments.')
    end

    %% Fix for Octave
    if is_octave
      pkg("load","image")
      pkg("load","optim")
    end

    %% Specify File and Parameters

    %MUTABLE HARDCODE PARAMETERS:
    %repetition number to choose if number of reps > 1;
    rep = 1;

    %grayscale threshold multiplier (useful range probably around 0.5 -
    %1.5), default 1.1
    finetune = 1.1;

    %radius of disk used by strel for ROI placed on each well (if 'disk' is specified in the 4th argument)
    diskradius = 5;

    %edge length of square used by strel for ROI placed on each well
    squaresize = 12;

    %offset applied to well centroid estimate
    %offset = [0 0]; %[10 0] offset for well cross-sections

	datadir %base directory for Bruker data
    scan %scan number to process
    UPSIDEDOWN  %set to 1 if the plate is upside down, for whatever reason

    if ~(strcmpi(ROIshape,'disk') || strcmpi(ROIshape,'square'))
        disp('Error: The ROI shape argument can be either ''disk'' or ''square''')
        return
    end

    %% Load Imaging Data

    %get scan parameters for the specified T2-weighted scan
    params = Fpv_get_params(datadir,scan);

    points_read = params.read;
    points_phase = params.phase;
    nechos = params.nechoes;
    Aver = params.na;
    TR = params.tr/1000;
    echotime = params.teinterval/1000;
    TE = echotime:echotime:(echotime*nechos);
    nslices = params.nslices;

    I=zeros(points_read, points_phase, 1);

    %get the intensity data
    %I = Fpv_msme_ft2dbc(datadir,scan,0,1);  %Fpv_msme_ft2d(datadir,expt(i));
    Iraw = Fpv_msme_ft2dbc09(datadir,scan,0,1); %for multislice data
    if nslices > 1
    	I = squeeze(Iraw(:,:,slice,:));
        clear Iraw
    else
        I = Iraw;
        clear Iraw
    end

    %averaging adjustments
    for i=1:length(Aver);
        I(:,:,i)=I(:,:,i)./Aver(i);
    end

    n1 = size(I,1);
    n2 = size(I,2);
    n3 = size(I,3);

    %resample to ensure square dims
    if points_read ~= points_phase
        resample_iso = max(points_read,points_phase);
        I = Fmat_resample(I,[resample_iso resample_iso n3]);
    end

    if UPSIDEDOWN == 0
        I = flip(flip(I,2),1);
    end

    n1 = size(I,1);
    n2 = size(I,2);

    %%  Find wells automatically by blob recognition and create a circular ROI
    %%  around each one
    %create a normalized version of the dataset for display

    Inorm = zeros(size(I));
    for i = 1:nechos
        if ndims(I) == 3
            %subtract baseline
            Inorm(:,:,i) = I(:,:,i) - min(min(I(:,:,i)));
            %normalize to max
            Inorm(:,:,i) = Inorm(:,:,i)./max(max(Inorm(:,:,i)));
        elseif ndims(I) == 4
            %subtract baseline
            Inorm(:,:,:,i) = I(:,:,:,i) - min(min(min(I(:,:,:,i))));
            %normalize to max
            Inorm(:,:,:,i) = Inorm(:,:,:,i)./max(max(max(Inorm(:,:,:,i))));
        end

    end

    %find well centroids from first image
    wells = FindWells(Inorm(:,:,1),finetune);
    wells = wells + repmat(offset,size(wells,1),1);
    wells = wells(wells(:,1) < n1 & wells(:,2) < n2,:);

    if strcmpi(ROIshape,'disk')
        SE = strel('disk',diskradius);
    elseif  strcmpi(ROIshape,'square')
        SE = strel('square',squaresize); %square ROI w/ edge = 10px
	end

    numwells = length(wells);
    ROI=zeros(n1,n2,numwells);

    %make a disk shaped mask around each well's centroid
    for y=1:numwells
        mask=zeros(n1,n2);
        mask(round(wells(y,2)),round(wells(y,1)))=1;
        mask=imdilate(mask,SE);
        ROI(:,:,y)=mask;
    end;

    ROIsum=zeros(n1,n2);
    for i=1:numwells
        ROIsum=ROIsum+ROI(:,:,i);
    end

    %% Select wells of interest

	f1 = figure;
	title('Select ROI rectangle')
    imshow(Inorm(:,:,round(nechos/2)) + Inorm(:,:,round(nechos/2)) .* 0.5 .* ROIsum);
%     imshow(Inorm(:,:,1) + Inorm(:,:,1) .* 0.5 .* ROIsum);
%     if strcmpi(ROIshape,'disk')
%         for y = 1:numwells
%             h(y) = rectangle('Position',[round(wells(y,1)-squaresize/2),round(wells(y,2)-squaresize/2),2*diskradius,2*diskradius],'Curvature',[1,1],'EdgeColor','b');
%         end
%     elseif  strcmpi(ROIshape,'square')
%         for y = 1:numwells
%             h(y) = rectangle('Position',[round(wells(y,1)-squaresize/2),round(wells(y,2)-squaresize/2),squaresize,squaresize],'EdgeColor','b');
%         end
% 	end

    % Add fix for missing Octave implementation
    if (is_octave)
      % prompt for input at command line
      x1 = input("Provide X coodinate of top-left corner of ROI: ");
      y1 = input("Provide Y coodinate of top-left corner of ROI: ");
      x2 = input("Provide X coodinate of bottom-right corner of ROI: ");
      y2 = input("Provide Y coodinate of bottom-right corner of ROI: ");
      rect = [x1, y1, x2-x1, y2-y1];
    else
      rect = getrect(gca);
    end

    chosenwells = find(wells(:,1) >= rect(1) & wells(:,1) <= rect(1) + rect(3) & wells(:,2) >= rect(2) & wells(:,2) <= rect(2) + rect(4));

    for i=1:length(chosenwells)
        text(wells(chosenwells(i),1),wells(chosenwells(i),2),num2str(chosenwells(i)),...
            'VerticalAlignment','middle',...
            'HorizontalAlignment','center',...
            'FontSize',7,...
            'Color','g');
    end

    %% Do masked background subtraction of raw intensities
    if BASELINE
        bgmask = ~imdilate(ROIsum,SE);
        for i = 1:nechos
            im = I(:,:,i);
            I(:,:,i) = I(:,:,i) - mean(mean(im(bgmask)));
        end
    end

    %% Extract Intensities

    w = zeros(numwells,n3);
    for i=1:numwells
        for j=1:n3
            im=I(:,:,j);
            roi=logical(ROI(:,:,i));
            w(i,j)=mean(im(roi));
        end
    end

    %DON'T normalize ROI intensity
    %w = w./max(max(w));


    %% T2 Fit across TE
%     if BASELINE
%         T2fn=inline('A(1).*(exp(-(t).*A(2)))+A(3)','A','t');
%     else
        T2fn=inline('A(1).*(exp(-(t).*A(2)))','A','t');
%     end

    TEcurve=(0:2:(echotime*nechos*1000))./1000;
    T2curveEVEN=zeros(numwells, length(TEcurve));
    T2curveODD=zeros(numwells, length(TEcurve));
	%% Fit ODD echos
%     if BASELINE
%         fitODD=zeros(nechos/2, 3);
%         for i=1:numwells;
%             fitODD(i,:)=nlinfit(TE(3:2:(nechos-1)), w(i,3:2:(nechos-1)), T2fn, [1E5,0.5,1E2],statset('FunValCheck','off','MaxIter',100,'Display','off'));
%             T2curveODD(i,:)=T2fn(fitODD(i,:), TEcurve);
%         end
%     else
        fitODD=zeros(nechos/2, 2);
        for i=1:numwells;
            fitODD(i,:)=nlinfit(TE(3:2:(nechos-1)), w(i,3:2:(nechos-1)), T2fn, [1E5,0.5],statset('FunValCheck','off','MaxIter',100,'Display','off'));
            T2curveODD(i,:)=T2fn(fitODD(i,:), TEcurve);
        end
%     end

    f2 = figure;

    plot(TE(1:2:nechos), w(chosenwells,1:2:nechos), 'o'); hold on;
    plot(TEcurve, T2curveODD(chosenwells,:), '-');
    title('Odd Fit')

    %% Fit EVEN echos
%     if BASELINE
%         fitEVEN=zeros(nechos/2, 3);
%         for i=1:numwells;
%             fitEVEN(i,:)=nlinfit(TE(4:2:(nechos-2)), w(i,4:2:(nechos-2)), T2fn, [1E5,0.5,1E2],statset('FunValCheck','off','MaxIter',100,'Display','off'));
%             T2curveEVEN(i,:)=T2fn(fitEVEN(i,:), TEcurve);
%         end
%     else
        fitEVEN=zeros(nechos/2, 2);
        for i=1:numwells;
            fitEVEN(i,:)=nlinfit(TE(4:2:(nechos-2)), w(i,4:2:(nechos-2)), T2fn, [1E5,0.5],statset('FunValCheck','off','MaxIter',100,'Display','off'));
            T2curveEVEN(i,:)=T2fn(fitEVEN(i,:), TEcurve);
        end
%     end

    f3 = figure;
    plot(TE(2:2:nechos), w(chosenwells,2:2:nechos), 'x'); hold on;
    plot(TEcurve, T2curveEVEN(chosenwells,:), '-');
    title('Even Fit')


    %% Fit ALL echos
%     if BASELINE
%         fitALL=zeros(nechos, 3);
%         for i=1:numwells;
%             fitALL(i,:)=nlinfit(TE(3:nechos), w(i,3:nechos), T2fn, [1E5,0.5,1E2],statset('FunValCheck','off','MaxIter',100,'Display','off'));
%             T2curveALL(i,:)=T2fn(fitALL(i,:), TEcurve);
%         end
%     else
        fitALL=zeros(nechos, 2);
        for i=1:numwells;
            fitALL(i,:)=nlinfit(TE(3:nechos), w(i,3:nechos), T2fn, [1E5,0.5],statset('FunValCheck','off','MaxIter',100,'Display','off'));
            T2curveALL(i,:)=T2fn(fitALL(i,:), TEcurve);
        end
%     end

    f4 = figure;
    plot(TE, w(chosenwells,:), 'x'); hold on;
    plot(TEcurve, T2curveALL(chosenwells,:), '-');
    title('All Fit')



    %%
    %%Plot out T2 on wells

    f5 = figure;
    imshow(Inorm(:,:,round(nechos/2)));
    for i=1:length(chosenwells)
        text(wells(chosenwells(i),1),wells(chosenwells(i),2),num2str(fitODD(chosenwells(i),2),4),...
            'VerticalAlignment','middle',...
            'HorizontalAlignment','center',...
            'FontSize',8,...
            'Color','r');
    end

    f6 = figure;
    imshow(Inorm(:,:,round(nechos/2)));
    for i=1:length(chosenwells)
        text(wells(chosenwells(i),1),wells(chosenwells(i),2),num2str(fitEVEN(chosenwells(i),2),4),...
            'VerticalAlignment','middle',...
            'HorizontalAlignment','center',...
            'FontSize',8,...
            'Color','r');
    end

    f7 = figure;
    imshow(Inorm(:,:,round(nechos/2)));
    for i=1:length(chosenwells)
        text(wells(chosenwells(i),1),wells(chosenwells(i),2),num2str(fitALL(chosenwells(i),2),4),...
            'VerticalAlignment','middle',...
            'HorizontalAlignment','center',...
            'FontSize',8,...
            'Color','r');
    end

    %[chosenwells fitODD(chosenwells,2) fitEVEN(chosenwells,2) fitALL(chosenwells,2)]
    oddR2 = fitODD(chosenwells,2);
    evenR2 = fitEVEN(chosenwells,2);
    R2 = fitALL(chosenwells,2);

    disp(' ')
    disp('Results:')
    disp(' ')
    disp('Wells     R2 odd     R2 even     R2 (1/sec)')
    filler = repmat('     ',size(chosenwells,1),1);
    disp([int2str(chosenwells) filler num2str(oddR2) filler num2str(evenR2) filler num2str(R2)])
    disp(' ')

    %tac onto datadir an OS-appropriate path slash
    if ~(datadir(length(datadir)) == '/' || datadir(length(datadir)) == '\')
        if strfind(computer,'PCWIN')
            datadir = [datadir '\'];
        else
            datadir = [datadir '/'];
        end
    end

    %Output the chosenwells wells and associated R1 values to a comma delimited
    %file within the data directory.  This will append if the file already
    %exists.
    disp(['Writing ' datadir num2str(scan) '-' num2str(slice) '_R2.csv'])
    dlmwrite([datadir num2str(scan) '-' num2str(slice) '_R2.csv'],[chosenwells oddR2 evenR2 R2],'delimiter',',');

    %Output figures to png image files within the specified datadir
    disp(['Writing ' datadir num2str(scan) '-' num2str(slice) '_wellsR2.png'])
    print(f1, '-dpng', [datadir num2str(scan) '-' num2str(slice) '_wellsR2.png']);
    disp(['Writing ' datadir num2str(scan) '-' num2str(slice) '_curvesoddR2.png'])
    print(f2, '-dpng', [datadir num2str(scan) '-' num2str(slice) '_curvesoddR2.png']);
    disp(['Writing ' datadir num2str(scan) '-' num2str(slice) '_curvesevenR2.png'])
    print(f3, '-dpng', [datadir num2str(scan) '-' num2str(slice) '_curvesevenR2.png']);
    disp(['Writing ' datadir num2str(scan) '-' num2str(slice) '_curvesR2.png'])
    print(f4, '-dpng', [datadir num2str(scan) '-' num2str(slice) '_curvesR2.png']);
    disp(['Writing ' datadir num2str(scan) '-' num2str(slice) '_fitsoddR2.png'])
    print(f5, '-dpng', [datadir num2str(scan) '-' num2str(slice) '_fitsoddR2.png']);
    disp(['Writing ' datadir num2str(scan) '-' num2str(slice) '_fitsevenR2.png'])
    print(f6, '-dpng', [datadir num2str(scan) '-' num2str(slice) '_fitsevenR2.png']);
    disp(['Writing ' datadir num2str(scan) '-' num2str(slice) '_fitsR2.png'])
    print(f7, '-dpng', [datadir num2str(scan) '-' num2str(slice) '_fitsR2.png']);

    %create globally normalized magnitude images and dump them to disk
    Igmin = min(min(min(I)));
    Igmax = max(max(max(I)));
    for i=1:nechos
    	Ignorm = (I(:,:,i) - Igmin)/(Igmax - Igmin);
        disp(['Writing ' datadir num2str(scan) '-' num2str(slice) '_mag_TE' num2str(TE(i)*1000) 'ms.png'])
        imwrite(Ignorm,[datadir num2str(scan) '-' num2str(slice)  '_mag_TE' num2str(TE(i)*1000) 'ms.png']);
    end

    disp('Done. Now you can go home.')
end

function r = is_octave ()
  persistent x;
  if (isempty (x))
    x = exist ('OCTAVE_VERSION', 'builtin');
  end
  r = x;
end
