function A = CMRI( varargin )
% Loads a bruker ('2dseq') file into matrix A
% First argument is file name
% second argument is matrix dimensions of the form [x y z t]
% aviadhai@mit.edu

  fname = varargin{1};

  fileID = fopen(fname);

  A = fread(fileID,'uint16');
  size(A)
  A = reshape(A,cell2mat(varargin(2)));
  fclose(fileID);
end