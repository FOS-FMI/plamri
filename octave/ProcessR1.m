% ProcessR1
%
% Fit longitudinal relaxation rates (R1) for a series of
% T1-weighted scans of multiwell plates, starting from the raw FID.
%
%
% Syntax:
% [R1 chosenwells] = ProcessR1(datadir, lastscan, numscans, [ROIshape], [upsidedown], [slice])
%
% Parameters:
%
% datadir = root directory name for Bruker data, given as a string, of
% course.
% e.g. 'C:\Users\Victor\Documents\My Dropbox\Lab Data
% (Jasanoff)\MRI\vl092512.g81'
%
% lastscan = the final scan number in a contiguous series of T1-weighted
% scans (can't recall why I did it this way... whatever.)
%
% numscans = the number of scans in the series
%
% ROIshape = either 'disk' or 'square'.  This option determines whether the
% function uses a disk-shaped or filled square-shaped ROI mask on
% recognized plate wells.  (OPTIONAL, DEFAULT = 'square')
%
% finetune = contrast parameter for well detection, usually between 0.5 and
% 1.5. (OPTIONAL, DEAFULT = 1.1)
%
% NOTE: The dimensions of these ROIs are sadly hard-coded right now.
% They're based on the usual ROI sizes for 384 circle-well or square-well
% plates with a 5 cm scan field-of-view.  Edit parameter lines to change.
%
% upsidedown = 0 or 1. Use this parameter if you put the
% plate into the scanner upside down.  This will clean up your little mess.
% (OPTIONAL, DEFAULT = 0);
%
% The function will automatically write a comma-delimted csv file in the
% data directory specified in the first parameter.  The first column is the
% assigned well number for recognized wells.  The second column is the R1
% rate estimate in 1/sec units.
%
% ----------------------------------------------------------------------
% Victor Lelyveld
% Jasanoff Lab
% 2012
% ----------------------------------------------------------------------

function [R1 chosenwells] = ProcessR1(datadir, lastscan, numscans, varargin)

    if nargin < 3
        disp('Error: Too few arguments.')
    end
    if nargin >= 4
        select = varargin{1};
    else
        select = 'interactive'; %DEFAULT to interactive selection
    end
    if nargin >= 5
        ROIshape = varargin{2};
    else
        ROIshape = 'square'; %DEFAULT to square shaped ROI
    end
    if nargin >= 6
        finetune = varargin{3};
    else
        finetune = 1.1; %DEFAULT to 1.1
    end
    if nargin >= 7
        UPSIDEDOWN = varargin{4};
    else
        UPSIDEDOWN = 0; %DEFAULT to right side up
    end
    if nargin == 8
        slice = varargin{5};
    else
        slice = 1;
    end
    if nargin > 7
        disp('Error: Too many arguments.')
    end

    %% Fix for Octave
    if is_octave
      pkg("load","image")
      pkg("load","optim")
    end

    %% Specify File and Parameters

    %MUTABLE HARDCODE PARAMETERS:
    %repetition number to choose if number of reps > 1;
    rep = 1;

    %radius of disk used by strel for ROI placed on each well (if 'disk' is specified in the 4th argument)
    diskradius = 5;

    %edge length of square used by strel for ROI placed on each well
    squaresize = 12;

    %echo command line parameters to console
    datadir %base directory for Bruker data
    lastscan %last T1-WEIGHTED scan number
    numscans %total number of scans (script will load data from lastscan:-1:(lastscan - numscans + 1)
    UPSIDEDOWN  %set to 1 if the plate is upside down, for whatever reason

    if ~(strcmp(ROIshape,'disk') || strcmp(ROIshape,'square'))
        disp('Error: The ROI shape argument can be either ''disk'' or ''square''')
    end

    expt = [lastscan - (0:(numscans - 1))]; %experiment (acquisition) number
    nechos = length(expt);

    %% Load Imaging Data

    Aver = []; %number of averages for each scan
    TR = [];  %TR in seconds
    params = Fpv_get_params(datadir,expt(1));
    points_read = params.read;
    points_phase = params.phase;
    nslices = params.nslices;
    I=zeros(points_read, points_phase, length(expt));

    %get scan parameters for each T1-weighted scan
    for i=1:length(expt);
        params = Fpv_get_params(datadir,expt(i));
        Aver = [Aver params.na];
        TR = [TR params.tr/1000];

        %get the intensity data
        %Iraw = Fpv_msme_ft2d(datadir,expt(i));
        %Iraw = Fpv_msme_ft2dbc(datadir,expt(i),0,1);  %for baseline correction
        Iraw = Fpv_msme_ft2dbc09(datadir,expt(i),0,1); %for multislice data

        if nslices > 1
            I(:,:,i) = Iraw(:,:,slice);
        else
            I(:,:,i) = Iraw;
            clear Iraw
        end
    end

    %averaging adjustments
    for i=1:length(Aver);
        I(:,:,i)=I(:,:,i)./Aver(i);
    end

    n1 = size(I,1);
    n2 = size(I,2);
    n3 = size(I,3);

    %resample to ensure square dims
    if points_read ~= points_phase
        resample_iso = max(points_read,points_phase);
        I = Fmat_resample(I,[resample_iso resample_iso n3]);
    end

    if UPSIDEDOWN == 0
        I = flip(flip(I,2),1);
    end

    n1 = size(I,1);
    n2 = size(I,2);

    %%  Find wells automatically by blob recognition and create a circular ROI
    %%  around each one
    %create a normalized version of the first dataset for display

    Inorm = zeros(size(I));
    for i = 1:nechos


        if ndims(I) == 3
            %subtract baseline
            Inorm(:,:,i) = I(:,:,i) - min(min(I(:,:,i)));
            %normalize to max
            Inorm(:,:,i) = Inorm(:,:,i)./max(max(Inorm(:,:,i)));
        elseif ndims(I) == 4
            %subtract baseline
            Inorm(:,:,:,i) = I(:,:,:,i) - min(min(min(I(:,:,:,i))));
            %normalize to max
            Inorm(:,:,:,i) = Inorm(:,:,:,i)./max(max(max(Inorm(:,:,:,i))));
        end

    end

    %find well centroids from first
    wells = FindWells(Inorm(:,:,nechos),finetune);

    if strcmp(ROIshape,'disk')
        SE = strel('disk',diskradius);
    elseif  strcmp(ROIshape,'square')
        SE = strel('square',squaresize); %square ROI w/ edge = 10px
    end
    numwells = length(wells);
    ROI=zeros(n1,n2,numwells);

    %make a disk shaped mask around each well's centroid
    for y=1:numwells
        mask=zeros(n1,n2);
        mask(round(wells(y,2)),round(wells(y,1)))=1;
        mask=imdilate(mask,SE);
        ROI(:,:,y)=mask;
    end;

    ROIsum=zeros(n1,n2);
    for i=1:numwells
        ROIsum=ROIsum+ROI(:,:,i);
    end

    %% Select wells of interest
    f1 = figure;
    imshow(Inorm(:,:,round(nechos/2)) + Inorm(:,:,round(nechos/2)) .* 0.5 .* ROIsum);
    if strcmp(select, 'all')
      rect = [0, 0, columns(ROIsum), rows(ROIsum)];
    elseif strcmp(select, 'interactive')
      title('Select ROI rectangle')
      if (is_octave)
        % fix for missing Octave implementation
        % prompt for input at command line
        x1 = input("Provide X coodinate of top-left corner of ROI: ");
        y1 = input("Provide Y coodinate of top-left corner of ROI: ");
        x2 = input("Provide X coodinate of bottom-right corner of ROI: ");
        y2 = input("Provide Y coodinate of bottom-right corner of ROI: ");
        rect = [x1, y1, x2-x1, y2-y1];
      else
        rect = getrect(gca);
      end
    else
      select = [select(1), select(2), select(3)-select(1), select(4)-select(2)];
      rect = select
    end

    chosenwells = find(wells(:,1) >= rect(1) & wells(:,1) <= rect(1) + rect(3) & wells(:,2) >= rect(2) & wells(:,2) <= rect(2) + rect(4));

    for i=1:length(chosenwells)
        text(wells(chosenwells(i),1),wells(chosenwells(i),2),num2str(chosenwells(i)),...
            'VerticalAlignment','middle',...
            'HorizontalAlignment','center',...
            'FontSize',7,...
            'Color','g');
    end

    %% Extract Intensities

    w = zeros(numwells,n3);
    for i=1:numwells
        for j=1:n3
            im=I(:,:,j);
            roi=logical(ROI(:,:,i));
            w(i,j)=mean(im(roi));
        end
    end

    %DON'T normalize ROI intensity
    %w = w./max(max(w));


    %% T1 Fit across ALL TR
    T1fn=inline('A(1).*(1-exp(-(t).*A(2)))','A','t');

    TRcurve=(0:2:max(TR.*1000))./1000;
    T1curveALL=zeros(numwells, length(TRcurve));
    fitALL=zeros(round(nechos/2), 3);
    warning off %this is dangerous but is usually due to a spurious well that isn't there
    for i=1:numwells
        fitALL(i,:)=nlinfit(TR, w(i,:), T1fn, [max(max(w)),0.5,0.001]);%,statset('FunValCheck','off','MaxIter',100,'Display','off'));
        T1curveALL(i,:)=T1fn(fitALL(i,:), TRcurve);
    end
    warning on

    f2 = figure;
    %plot(TR, w, 'o'); hold on;
    plot(TR(1:nechos), w(chosenwells,1:nechos)', 'x');
    hold on;
    plot(TRcurve, T1curveALL(chosenwells,:));
    title('Complete Fit')


    %%Plot out T1 on wells
    f3 = figure;
    %Fmat_disp2(Inorm(:,:,round(nechos/2)),Inorm(:,:,round(nechos/2)).* (1-ROIsum));
    imshow(Inorm(:,:,round(nechos/2)));
    for i=1:length(chosenwells)
        text(wells(chosenwells(i),1),wells(chosenwells(i),2),num2str(fitALL(chosenwells(i),2),3),...
            'VerticalAlignment','middle',...
            'HorizontalAlignment','center',...
            'FontSize',8,...
            'Color','r');
    end

    R1 = fitALL(chosenwells,2);

    disp(' ')
    disp('Results:')
    disp(' ')
    disp('Wells     Fit R1 (1/sec)')
    disp([int2str(chosenwells) repmat('     ',size(chosenwells,1),1) num2str(R1)])
    disp(' ')

    %tac onto datadir an OS-appropriate path slash
    if ~(datadir(length(datadir)) == '/' || datadir(length(datadir)) == '\')
        if strfind(computer,'PCWIN')
            datadir = [datadir '\'];
        else
            datadir = [datadir '/'];
        end
    end

    %Output the chosen wells and associated R1 values to a comma delimited
    %file within the data directory.  This will append if the file already
    %exists.
    disp(['Writing ' datadir 'R1.csv'])
    r1_header = 'plamri_id,R1';
    fid = fopen([datadir 'R1.csv'],'w');
    fprintf(fid, '%s\n', r1_header)
    fclose(fid)
    dlmwrite([datadir 'R1.csv'],[chosenwells R1],'delimiter',',','-append');

    %Output figures to png image files within the specified datadir
    disp(['Writing ' datadir 'wellsR1.png'])
    print(f1, '-dpng', [datadir 'wellsR1.png']);
    disp(['Writing ' datadir 'curvesR1.png'])
    print(f2, '-dpng', [datadir 'curvesR1.png']);
    disp(['Writing ' datadir 'fitsR1.png'])
    print(f3, '-dpng', [datadir 'fitsR1.png']);

    %create globally normalized magnitude images and dump them to disk
    Igmin = min(min(min(I)));
    Igmax = max(max(max(I)));
    for i=1:nechos
    	Ignorm = (I(:,:,i) - Igmin)/(Igmax - Igmin);
        disp(['Writing ' datadir 'mag_TR' num2str(TR(i)*1000) 'ms.png'])
        print(Ignorm, '-dpng', [datadir 'mag_TR' num2str(TR(i)*1000) 'ms.png']);
    end

    disp('Done. Now you can get dinner.')
end

function r = is_octave ()
  persistent x;
  if (isempty (x))
    x = exist ('OCTAVE_VERSION', 'builtin');
  end
  r = x;
end
