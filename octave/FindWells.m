%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VICTOR LELYVELD - 11172008
%
% FindWells - Designed to find multiwell plate wells in MRI images
%   automatically and return the centroid of each identified well.
%   Currently, the script doesn't do anything smart to distinguish
%   wells from artifacts (or much of anything smart in general).
%
%   Input: Grayscale image, finetune threshold parameter
%   Output: Array of blob centroid coordinates
%
%   finetune parameter's useful values are probably around 1 - 1.5 or so.
%   I would only change this if you're getting a lot of incorrect well
%   assignments.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function centroids = FindWells(GrayImage, varargin)

pkg load image

if nargin > 1
    finetune = varargin{1};
else
    finetune = 1.1; %default
end

% Threshold the image
threshold = graythresh(GrayImage);
BWimage = im2bw(GrayImage, finetune*threshold);

% Invert the Binary Image
BWimage = ~ BWimage;

% Find the boundaries Concentrate only on the exterior boundaries.
% Option 'noholes' will accelerate the processing by preventing
% bwboundaries from searching for inner contours.
[B,L] = bwboundaries(BWimage);%, 'noholes');

% Get properties of all blobs
BlobInfo = regionprops(L, 'all');

% figure;
% imshow(BWimage);
% title('Found Blobs!');
% hold on

centroids = zeros(length(BlobInfo),2);
for i = 1 : length(BlobInfo)
  centroids(i,:) = BlobInfo(i).Centroid;

  %use this for well bottoms in saggital or axial slices
  %centroids(i,1) = BlobInfo(2).Centroid(1) + BlobInfo(2).BoundingBox(3)/2 - 5;

  %plot(centroids(i,1),centroids(i,2),'wO');
end
return
