
% ****************************************************
% ***** Open and read parameters for Bruker data *****
% ****************************************************

   function d = Fpv_get_params(name, num)

   number     = num2str(num);
   fidname    = strcat(name,'/',number,'/fid');
   acqpname   = strcat(name,'/',number,'/acqp');

   disp(fidname);

   fid        = fopen(fidname,'r','l');
   acqp       = fopen(acqpname,'rt');

   if (fid == -1)
      error('Data not found');
   end

   epi = 0;
   while (feof(acqp) == 0)
      line = fgetl(acqp);
      if (isempty(findstr(line,'$ACQ_size')) == 0)
         line = fgetl(acqp);
         s = sscanf(line,'%d');
      end
      if (isempty(findstr(line,'$L=')) == 0)
         line = fgetl(acqp);
         l = sscanf(line,'%d');
      end
      if (isempty(findstr(line,'$ACQ_obj_order=')) == 0)
         line = fgetl(acqp);
         order = sscanf(line,'%d');
      end
      if (isempty(findstr(line,'$NECHOES')) == 0)
         nechoes = sscanf(line,'##$NECHOES= %d',1);
      end;
      if (isempty(findstr(line,'$NSLICES')) == 0)
         nslices = sscanf(line,'##$NSLICES= %d',1);
      end;
      if (isempty(findstr(line,'$NR')) == 0)
         nr = sscanf(line,'##$NR= %d',1);
      end;
      if (isempty(findstr(line,'$NA=')) == 0)
         na = sscanf(line,'##$NA= %d',1);
      end;
      if (isempty(findstr(line,'$ACQ_repetition_time')) == 0)
          line = fgetl(acqp);
          tr = sscanf(line,'%d');
      end;

      if (isempty(findstr(line,'$ACQ_inter_echo_time')) == 0)
          line = fgetl(acqp);
          teinterval = sscanf(line,'%d');
      end;
   end

   d.read = s(1)/2;
   d.phase = s(2);
   d.nslices = nslices;
   d.nechoes = nechoes;
   d.nr = nr;
   d.na = na;
   d.tr = tr;
   d.teinterval = teinterval;

   disp(strcat(name,', experiment #',number));
   disp(strcat('   points in read dimension (d1):',num2str(d.read)));
   disp(strcat('            phase dimension (d2):',num2str(d.phase)));
   disp(strcat('            slice dimension (d3):',num2str(d.nslices)));
   disp(strcat('      number of echo images (d4):',num2str(d.nechoes)));
   disp(strcat('      number of repetitions (d5):',num2str(d.nr)));
   disp(strcat('         number of averages (d6):',num2str(d.na)));
   disp(strcat('                         TR (d7):',num2str(d.tr')));
   disp(strcat('                         TE (d8):',num2str(d.teinterval)));

   fclose('all');
