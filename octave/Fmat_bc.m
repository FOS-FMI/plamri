% *************************************************
% ***** Shift matrix by user-specified vector *****
% *************************************************

   function I = Fmat_bc(mat,dim,frac);

   if (nargin == 0)
      error('Format: I = Fmat_bc(mat, dimension, fraction)');
   end;

% baseline correct data matrix

   if (dim == 1)
      last = floor(size(mat,1)*frac);
      bc   = sum(mat(1:last,:,:),1)/last;
      for i=1:size(mat,1)
         mat(i,:,:) = mat(i,:,:) - bc;
      end;
   end;

   if (dim == 2)
      last = floor(size(mat,2)*frac);
      bc   = sum(mat(:,1:last,:),2)/last;
      for i=1:size(mat,2)
         mat(:,i,:) = mat(:,i,:) - bc;
      end;
   end;

   if (dim == 3)
      last = floor(size(mat,3)*frac);
      bc   = sum(mat(:,:,1:last),3)/last;
      for i=1:size(mat,3)
         mat(:,:,i) = mat(:,:,i) - bc;
      end;
   end;

   I = mat;
