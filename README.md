# PlaMRI

PlaMRI (Plate Magnetic Resonance Imaging) provides MRI preprocessing, metadata parsing, and data analysis functions and workflows for plate scanning.
Functions currently available include an Octave implementation by Victor Lelyveld as well as an emerging Python package.

Both the Octave and Python versions are compatible with data acquired via Bruker ParaVision.

## Installation
The Python package can be installed and used directly via the Python interpreter; the Octave implementation is run from the source directory.

#### Python Package Manager (Developers):
Python's `setuptools` allows you to install Python packages independently of your distribution (or operating system, even);
it also allows you to install a "live" version of the package - dynamically linking back to the source code.
This permits you to test code (with real module functionality) as you develop it.
As the Python package, is in early development this is currently the only recommended install option.

````
git clone git@gitlab.com:FOS-FMI/plamri.git
cd plamri
echo "export PATH=\$HOME/.local/bin/:\$PATH" >> ~/.bashrc
source ~/.bashrc
python setup.py develop --user
````

If you are getting a `Permission denied (publickey)` error upon trying to clone, you can either:

* Add an SSH key to your GitLab
* Pull via the HTTPS link `git clone https://github.com/IBT-FMI/SAMRI.git`.

## Dependencies

### Testing
The test data archive can be downloaded from [here](http://resources.chymera.eu/distfiles/plamri_brudata-0.1.tar.xz) and should be installed under `/usr/share/plamri_brudata`.

## Contributing

Contributions to both enhancing the non-graphical operation of the Octave package, as well as to porting the functionality to Python, are appreciated.
