import os
import re

PV_EXTRACTION_DICTS=[
	{'field_name':'frequency_encoding',
		'query_file':'acqp',
		'regex':r'^##\$ACQ_size=\( .*? \)$\n^(?P<value>.*?) .*?$'
		},
	{'field_name':'phase_encoding',
		'query_file':'acqp',
		'regex':r'^##\$ACQ_size=\( .*? \)$\n^.*? (?P<value>.*?)$'
		},
	{'field_name':'obj_order',
		'query_file':'acqp',
		'regex':r'^##\$ACQ_obj_order=\( .*? \)$\n^(?P<value>.*?)$'
		},
	{'field_name':'nechoes',
		'query_file':'acqp',
		'regex':r'^##\$NECHOES=(?P<value>.*?)$'
		},
	{'field_name':'nslices',
		'query_file':'acqp',
		'regex':r'^##\$NSLICES=(?P<value>.*?)$'
		},
	{'field_name':'nr',
		'query_file':'acqp',
		'regex':r'^##\$NR=(?P<value>.*?)$'
		},
	{'field_name':'na',
		'query_file':'acqp',
		'regex':r'^##\$NA=(?P<value>.*?)$'
		},
	{'field_name':'tr',
		'query_file':'acqp',
		'regex':r'^##\$ACQ_repetition_time=\( .*? \)$\n^(?P<value>.*?)$'
		},
	{'field_name':'teinterval',
		'query_file':'acqp',
		'regex':r'^##\$ACQ_inter_echo_time=\( .*? \)$\n^(?P<value>.*?)$'
		},
	]

def get_scan_params(in_path,
	verbose=False,
	):
	"""Parse scan parameters from ParaVision files

	Parameters
	----------
	in_path : str
		Path to a ParaVision scan directory (this is one level deeper than the session directory, and ends in a number)
	verbose : bool, optional
		Whether to report parameters.
	"""

	scan_params = {}
	for i in PV_EXTRACTION_DICTS:
		query_file = os.path.join(in_path,i['query_file'])
		regex = i['regex']
		rx_sequence=re.compile(regex,re.MULTILINE)

		with open(query_file,'r') as file_str:
			file_str = file_str.read()
			for match in rx_sequence.finditer(file_str):
				value = match.groupdict()['value']
				scan_params[i['field_name']] = value

	if verbose:
		for key, value in scan_params.items():
			print(key, ' : ', value)

	return scan_params
