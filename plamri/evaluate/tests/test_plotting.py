def test_plot_r1():
	from plamri.evaluate.plotting import plot_r1
	plot_r1('~/j_data/plates/plate4.6x2/',
		from_octave=True,
		verbose=True,
		report=True,
		save_as='plate4.png',
		)
	plot_r1('~/j_data/plates/plate5.6x1/',
		from_octave=True,
		verbose=True,
		report=True,
		save_as='plate5.png',
		)
	plot_r1('~/j_data/plates/plate7.701/',
		from_octave=True,
		verbose=True,
		report=True,
		save_as='plate7.png',
		)
