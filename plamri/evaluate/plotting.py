import os
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import seaborn as sns

def plot_r1(data_dir,
	from_octave=False,
	verbose=True,
	info_file_name='plate_contents.csv',
	save_as=False,
	report=False,
	sensor_concentration='bm3h-9d7-concentration',
	substrate_concentration='da-concentration',
	saturation_cutoff=1000,
	):

	lines_markersize = mpl.rcParams['lines.markersize']
	#mpl.rcParams['figure.figsize'] = 0.7,0.27

	data_dir = os.path.abspath(os.path.expanduser(data_dir))
	info_file = os.path.join(data_dir,info_file_name)
	if from_octave:
		r1_file = os.path.join(data_dir,'R1.csv')

	info_df = pd.read_csv(info_file)
	r1_df = pd.read_csv(r1_file)

	loaded_wells = info_df['plamri_id'].tolist()
	r1_background = r1_df.loc[~r1_df['plamri_id'].isin(loaded_wells)]
	background_median = r1_background.median()['R1'].item()

	target_info_df = info_df.loc[info_df['plamri_id'].isin(loaded_wells)]
	target_r1_df = r1_df.loc[r1_df['plamri_id'].isin(loaded_wells)]
	target_r1_df['R1'] = target_r1_df['R1'] - background_median

	df = target_info_df.join(target_r1_df.set_index('plamri_id'),on='plamri_id')
	if verbose:
		print(df)
	#sns.set(rc={"figure.figsize":(0.5, 0.5)}) #width=3, #height=4
	ax = sns.lmplot(
		x="da-concentration",
		y="R1",
		hue='bm3h-9d7-concentration',
		data=df,
		#scatter_kws={"s": 80},
		#order=1,
		logistic=True,
		#lowess=True,
		#robust=True,
		#logx=True,
		ci=None,
		)
	#ax.set(xscale="log")
	if save_as:
		plt.savefig(save_as)
	if report:
		r1 = df.loc[(df[substrate_concentration]==0)&(df[sensor_concentration]!=0)]
		r1['r1'] = r1['R1']*1000/r1[sensor_concentration]
		print('r1 = ',r1['r1'].tolist())
		r0 = df.loc[(df[substrate_concentration]>=saturation_cutoff)&(df[sensor_concentration]!=0)]
		r0['r0'] = r0['R1']*1000/r0[sensor_concentration]
		print('r0 = ',r0['r0'].tolist())
