import os

from plamri.paravision import get_scan_params

def process_r1(data_dir,
	explicit_scans=False,
	verbose=False,
	):
	"""Process R1 from a list of T1 weighted scans

	Parameters
	----------
	data_dir : str
		Path to paravision session directory
	explicit_scans : str or list, optional
		Either a list of scan, or a string containing two numbers (first and last scan, including the numbers themselves) separated by two periods (e.g. "2..12").
	"""

	if '..' in explicit_scans:
		first_scan, last_scan = explicit_scans.split('..')
		first_scan = int(first_scan)
		last_scan = int(last_scan)
		scans = [i for i in range(first_scan,last_scan+1)]
	scan_paths = [os.path.abspath(os.path.expanduser(os.path.join(data_dir,str(i)))) for i in scans]
	if verbose:
		print(scan_paths)
	for scan_path in scan_paths:
		params = get_scan_params(scan_path)

