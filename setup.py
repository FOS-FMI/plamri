from setuptools import setup, find_packages

packages = find_packages(exclude=('plamri.tests*'))

setup(
        name='PlaMRI',
        version=9999,
        descriiption='Plate MRI evaluation scripts',
        author='Horea Christian',
        author_email='chr@chymera.eu',
        url='https://gitlab.com/FOS-FMI/plamri',
        provides = ['plamri'],
        packages = packages,
        )
